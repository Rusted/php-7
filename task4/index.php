<html>
<?php
require_once 'functions.php';
$cssBase = 'public/css/';
$title = 'Hello world';
$cssFiles = ['style.css', 'theme.css'];
outputHead($title, $cssBase, $cssFiles);
?>
<body>
    Hello world!
</body>
</html>

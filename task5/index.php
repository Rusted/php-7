<?php
require_once 'adverts.php';

function sortByField($a, $b): int
{
    $sortKey = $_GET['sortByField'];
    if ($a[$sortKey] == $b[$sortKey]) {
        return 0;
    }

    return ($a[$sortKey] < $b[$sortKey]) ? -1 : 1;
}

if (isset($_GET['sortByField'])) {
    if (array_key_exists($_GET['sortByField'], $adverts[0])) {
        usort($adverts, 'sortByField');
    }

}

$totalAdverts = count($adverts);

$totalPaidAdverts = 0;
$totalPaidAdvertsSum = 0;
$totalUnpaidAdvertsSum = 0;
foreach ($adverts as $advert) {
    if ($advert['onPay']) {
        $totalPaidAdverts++;
        $totalPaidAdvertsSum += $advert['cost'];
    } else {
        $totalUnpaidAdvertsSum += $advert['cost'];
    }
}
?>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonym ous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</head>
<body class="container">
<h1>Skelbimai</h1>
    <div class="well">
        Viso skelbimų: <?php echo $totalAdverts; ?><br>
        Viso apmokėtų skelbimų: <?php echo $totalPaidAdverts; ?><br>
        Apmokėtų skelbimų suma: <?php echo $totalPaidAdvertsSum; ?><br>
        Neapmokėtų skelbimų suma: <?php echo $totalUnpaidAdvertsSum; ?><br>
    </div>
    <table class="table">
        <tr>
            <th class="title-id">Id</th>
            <th class="title-text">Tekstas</th>
            <th class="title-cost">Kaina</th>
            <th class="title-onpay">Paid on</th>
        </tr>
        <?php foreach ($adverts as $advert) {?>
        <tr>
            <th><?php echo $advert['id']; ?></th>
            <th><?php echo $advert['text']; ?></th>
            <th><?php echo $advert['cost']; ?></th>
            <th><?php echo $advert['onPay'] ? date('Y-m-d', $advert['onPay']) : ''; ?></th>
        </tr>
        <?php }?>
    </table>
    <script>
        $('.title-onpay').click(function() {
            sortByField('onPay');
        });

        $('.title-text').click(function() {
            sortByField('text');
        });

        $('.title-cost').click(function() {
            sortByField('cost');
        });

        $('.title-id').click(function() {
            sortByField('id');
        });

        function sortByField(fieldName) {
            var newForm = $('<form>', {
            }).append(jQuery('<input>', {
                'name': 'sortByField',
                'value': fieldName,
                'type': 'hidden'
            }));

            $(document.body).append(newForm);
            newForm.submit();
        }


    </script>
</body>

</html>
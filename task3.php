<?php declare (strict_types = 1);

function isLeapYear(int $year): bool
{
    return checkdate(2, 29, $year);
}

if (isset($_GET['year'])) {
    $year = $_GET['year'];
    $result = isLeapYear((int) $year);
}

?>
<html>

<body>
    <h1>Metai - keliamieji ar ne?</h1>
    <?php if (isset($result)) {?>
    <p>
        Rezultatas: <?php echo $result ? 'Keliamieji' : 'Nėra keliamieji'; ?>
    </p>
    <?php }?>
    <form>
        <input type="text" name="year" value="<?php echo isset($_GET['year']) ? $_GET['year'] : '' ?>">
        <input type="submit" value="Skaičiuoti">
    </form>
</body>

</html>